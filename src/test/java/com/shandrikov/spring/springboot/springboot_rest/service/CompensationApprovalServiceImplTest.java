package com.shandrikov.spring.springboot.springboot_rest.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class CompensationApprovalServiceImplTest {
    @Mock
    private CompensationMapper compensationMapper;
    @Mock
    private CompensationRepository compensationRepository;
    @Mock
    private EmployeeRepository employeeRepository;
    @Mock
    private CompensationStorageService compensationStorageService;

    @InjectMocks
    private CompensationApprovalServiceImpl compensationApprovalService;

    @Test
    public void updateCompensationStatusById_StatusApproved_Pass() {
        CompensationEntity compensationToChange = CompensationEntity.builder()
                .status(CompensationStatus.NOT_APPROVED)
                .startDate(LocalDate.now().minusDays(1))
                .build();
        CompensationStatusInfo compensationStatusInfo = new CompensationStatusInfo();
        compensationStatusInfo.setStatus(CompensationStatus.APPROVED);
        when(compensationRepository.findOne(anyLong())).thenReturn(compensationToChange);

        compensationApprovalService.updateCompensationStatusById(compensationStatusInfo);

        ArgumentCaptor<CompensationEntity> captor = ArgumentCaptor.forClass(CompensationEntity.class);
        verify(compensationRepository).save(captor.capture());
        Assert.assertEquals(LocalDate.now(), captor.getValue().getApproveDate());
        Assert.assertEquals("APPROVED", captor.getValue().getStatus().toString());
    }

    @Test
    public void updateCompensationStatusById_StatusNotApproved_DefaultApproveDate() {
        CompensationEntity compensationToChange = CompensationEntity.builder()
                .status(CompensationStatus.NOT_APPROVED)
                .startDate(LocalDate.now().minusDays(1))
                .build();
        CompensationStatusInfo compensationStatusInfo = new CompensationStatusInfo();
        compensationStatusInfo.setStatus(CompensationStatus.NOT_APPROVED);
        when(compensationRepository.findOne(anyLong())).thenReturn(compensationToChange);

        compensationApprovalService.updateCompensationStatusById(compensationStatusInfo);

        ArgumentCaptor<CompensationEntity> captor = ArgumentCaptor.forClass(CompensationEntity.class);
        verify(compensationRepository).save(captor.capture());
        Assert.assertNull(captor.getValue().getApproveDate());
        Assert.assertEquals("NOT_APPROVED", captor.getValue().getStatus().toString());
    }

    @Test
    public void updateCompensationStatusById_NotCurrentYear_DoesntSave() {
        CompensationEntity compensationToChange = CompensationEntity.builder()
                .startDate(LocalDate.now().minusYears(1))
                .build();
        CompensationStatusInfo compensationStatusInfo = new CompensationStatusInfo();
        when(compensationRepository.findOne(anyLong())).thenReturn(compensationToChange);

        compensationApprovalService.updateCompensationStatusById(compensationStatusInfo);

        verify(compensationRepository, never()).save(any(CompensationEntity.class));
    }

    @Test
    public void updateCompensationStatusById_Null_DoesntSave() {
        when(compensationRepository.findOne(anyLong())).thenReturn(null);
        CompensationStatusInfo compensationStatusInfo = new CompensationStatusInfo();

        compensationApprovalService.updateCompensationStatusById(compensationStatusInfo);

        verify(compensationRepository, never()).save(any(CompensationEntity.class));
    }

    @Test
    public void findCompensationsOfAllCompany_ValidEntities_ReturnList() {
        List<CompensationEntity> actualList = new ArrayList<>();
        actualList.add(new CompensationEntity());
        actualList.add(new CompensationEntity());
        actualList.add(new CompensationEntity());

        EmployeeEntity tempEmp = EmployeeEntity.builder()
                .surname("ashan")
                .name("tom")
                .build();

        Map<String, String> tempMap = new HashMap<>();
        tempMap.put("1", "1");

        CompensationFileEntity tempFileEntity = CompensationFileEntity.builder()
                .fileName("file")
                .build();

        List<CompensationFileEntity> tempList = new ArrayList<>();
        tempList.add(tempFileEntity);

        when(compensationRepository.findAll()).thenReturn(actualList);
        when(compensationMapper.fromEntityToDTO(any(CompensationEntity.class))).thenReturn(new CompensationDTO());
        when(employeeRepository.findById(anyLong())).thenReturn(tempEmp);
        when(compensationStorageService.findAllByCompensationId(anyLong())).thenReturn(tempList);
        when(compensationStorageService.getUrlToFileName(anyLong(), anyLong())).thenReturn(tempMap);

        List<CompensationDTO> compensationsOfAllCompany = compensationApprovalService.findCompensationsOfAllCompany();

        verify(compensationMapper, times(3)).fromEntityToDTO(any());
        Assert.assertEquals("ashan tom", compensationsOfAllCompany.get(0).getEmployeeName());
        Assert.assertEquals("file", compensationsOfAllCompany.get(0).getFileStorageNames().get(0));
        Assert.assertEquals(tempMap, compensationsOfAllCompany.get(0).getUrlToFileName());
    }

}
