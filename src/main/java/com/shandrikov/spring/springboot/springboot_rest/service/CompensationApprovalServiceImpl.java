package com.shandrikov.spring.springboot.springboot_rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service("CompensationApprovalService")
public class CompensationApprovalServiceImpl implements CompensationApprovalService {
    @Autowired
    private CompensationMapper compensationMapper;
    @Autowired
    private CompensationRepository compensationRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private CompensationStorageService compensationStorageService;


    @Override
    @Transactional
    public void updateCompensationStatusById(CompensationStatusInfo compensationStatusInfo) {
        CompensationEntity compensationToChange = compensationRepository.findOne(compensationStatusInfo.getId());
        int currentYear = LocalDate.now().getYear();
        if ((compensationToChange != null) && (currentYear == compensationToChange.getStartDate().getYear())) {
            compensationToChange.setStatus(compensationStatusInfo.getStatus());
            if (compensationToChange.getStatus() == CompensationStatus.APPROVED)
                compensationToChange.setApproveDate(LocalDate.now());
            compensationRepository.save(compensationToChange);
        }
    }


    @Override
    public List<CompensationDTO> findCompensationsOfAllCompany() {
        List<CompensationDTO> compensationsToApprove = StreamSupport.stream(compensationRepository.findAll().spliterator(), false)
                .map(compensation -> compensationMapper.fromEntityToDTO(compensation))
                .collect(Collectors.toList());
        compensationsToApprove.forEach(compensationDTO -> compensationDTO.setEmployeeName(employeeRepository.findById(compensationDTO.getEmployeeId()).getSurnameAndName()));
        compensationsToApprove.forEach(compensationDTO -> compensationDTO.setFileStorageNames(compensationStorageService.findAllByCompensationId(compensationDTO.getId()).stream()
                .map(CompensationFileEntity::getFileName)
                .collect(Collectors.toList())));
        compensationsToApprove.forEach(compensationDTO -> compensationDTO.setUrlToFileName(compensationStorageService.getUrlToFileName(compensationDTO.getEmployeeId(), compensationDTO.getId())));
        return compensationsToApprove;
    }

}
